FROM tensorflow/tensorflow:2.3.0-gpu-jupyter

ENV LANG C.UTF-8

LABEL maintainer="alessander.smirnov@gmail.com"

ARG WORKDIRECTORY=/var/nnetrunner/nnetrunner

RUN apt -y update

# check our python environment
RUN python3 --version
RUN pip3 --version

RUN pip3 install --upgrade pip
RUN pip3 install librosa
RUN pip3 install pandas
RUN pip3 install flask
RUN pip3 install numba==0.48
RUN pip3 install -U Werkzeug
RUN apt -y install libsndfile1
RUN pip3 install -U scikit-learn==0.22.1
RUN pip3 install gTTS
RUN pip3 install tensorflow_hub

RUN mkdir WORKDIRECTORY

WORKDIR WORKDIRECTORY

COPY nnetrunner/ /var/nnetrunner/
RUN ls -la /var/nnetrunner/*

# start app
CMD cd /var/nnetrunner/nnetrunner/ && python app.py