from tensorflow.keras.models import load_model, Model
from tensorflow.keras.layers import *
from tensorflow.keras import regularizers
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences

import tensorflow as tf
import pandas as pd
import librosa
import time
from joblib import dump, load
import numpy as np
from sklearn.preprocessing import MinMaxScaler

# load additional data
def getScaller(scalerPath):
    return load(scalerPath)

def getCategoriesMap(mapPath):
    return list(np.load(mapPath))

def getAppealsTokenizer(path):
    df = pd.read_csv(path)
    df = df.iloc[:, :2]
    texts = df['text'].values
    threshold_to_use = 200
    categories_to_use_counts = []
    for cl in df['category'].unique():
      count = df[df.category == cl].shape[0]
      if count >= threshold_to_use:
        categories_to_use_counts.append(cl)

    filtered_texts = [] 
    length = df['category'].values.shape[0]

    for i in range(0, length):
      if df['category'].values[i] in categories_to_use_counts:
        filtered_texts.append(df['text'].values[i])

    maxWordsCount = 60000
    tokenizer = Tokenizer(num_words=maxWordsCount,
                      filters='!"#$%&()*+,-./:;<=>?@[\\]^_`{|}~\t\n',
                      lower=True, split=' ',
                      oov_token='unknown',
                      char_level=False)
    tokenizer.fit_on_texts(filtered_texts)
    return tokenizer

# load sound definition models
def getSoundDefinitionModel(modelPath):
    return load_model(modelPath)

def getAppealClassificationModel(modelPath):
    filter_sizes = [3,4,5]
    num_filters = 100
    nClasses = 10
    drop = 0.5
    maxlen = 300
    maxWordsCount = 60000

    inputs = Input(shape=(maxlen,))
    embedding = Embedding(maxWordsCount, 300, input_length=maxlen)(inputs)

    reshape = Reshape((maxlen,maxlen,1))(embedding)

    conv_0 = Conv2D(num_filters, (filter_sizes[0], maxlen),activation='relu',kernel_regularizer=regularizers.l2(0.01))(reshape)
    conv_1 = Conv2D(num_filters, (filter_sizes[1], maxlen),activation='relu',kernel_regularizer=regularizers.l2(0.01))(reshape)
    conv_2 = Conv2D(num_filters, (filter_sizes[2], maxlen),activation='relu',kernel_regularizer=regularizers.l2(0.01))(reshape)

    maxpool_0 = MaxPooling2D((maxlen - filter_sizes[0] + 1, 1), strides=(1,1))(conv_0)
    maxpool_1 = MaxPooling2D((maxlen - filter_sizes[1] + 1, 1), strides=(1,1))(conv_1)
    maxpool_2 = MaxPooling2D((maxlen - filter_sizes[2] + 1, 1), strides=(1,1))(conv_2)

    merged_tensor = concatenate([maxpool_0, maxpool_1, maxpool_2], axis=1)
    flatten = Flatten()(merged_tensor)
    reshape = Reshape((3*num_filters,))(flatten)
    dropout = Dropout(drop)(flatten)
    output = Dense(nClasses, activation='softmax', kernel_regularizer=regularizers.l2(0.01))(dropout)

    model = Model(inputs, output)

    model.compile(optimizer='adam', 
              loss='categorical_crossentropy', 
              metrics=['accuracy'])
    model.summary()

    model.load_weights(modelPath)

    return model

# appeals classification
def getAppealCategoryFromText(model, categories, text, tokenizer):
    maxlen = 300

    test_sequences = tokenizer.texts_to_sequences([text])
    x_eval = np.array(test_sequences)
    x_eval = pad_sequences(x_eval, maxlen=maxlen)
    
    with tf.device('/cpu:0'):
        y_pred = model.predict(x_eval)
    
    winner = np.argmax(y_pred)
    result = categories[winner]
    return result

# getting features
def get_input_for_models(audioDataPath, needCalcLowDetailedFeatures):
    start = time.time()
    y, sr = librosa.load(audioDataPath, mono=True, duration=3)
    features = get_feature_matrix(y, sr)
    lowDetailedFeatures = []
    if needCalcLowDetailedFeatures:
        lowDetailedFeatures = get_low_detailed_features(y, sr)
    
    end = time.time()
    return (features, lowDetailedFeatures, end - start)


def get_feature_matrix(y, sr):
    mfcc = librosa.feature.mfcc(y=y, sr=sr,n_mfcc=64)
    return mfcc

def get_low_detailed_features(y, sr):
    (zcr, meanamp, tempo) = (0, 0, 0)
    try:
        zcr = np.mean(librosa.feature.zero_crossing_rate(y)) # среднее частота пересечения нуля звукового временного ряда
        meanamp = np.mean(y) * 10**4 # средняя амплитуда звука в интервале от -1 до 1
        oenv = librosa.onset.onset_strength(y, sr=sr)
        tempo = librosa.beat.tempo(onset_envelope=oenv, sr=sr)[0] # общий темп звука
    except:
        pass
    return {'zero_crossing_accuracy': str(zcr), 'mean_amplitude' : str(meanamp), 'tempo' : str(tempo)}

def get_features(y, sr, model_names): # obsolete
    chroma_stft = librosa.feature.chroma_stft(y=y, sr=sr)
    mfcc = librosa.feature.mfcc(y=y, sr=sr)
    spec_cent = np.mean(librosa.feature.spectral_centroid(y=y, sr=sr))
    spec_bw = np.mean(librosa.feature.spectral_bandwidth(y=y, sr=sr))
    rolloff = np.mean(librosa.feature.spectral_rolloff(y=y, sr=sr))
    zcr = np.mean(librosa.feature.zero_crossing_rate(y))
  
    features_for_models = {}

    out = []
    isvoiceout = []

    out.append(spec_cent)
    out.append(spec_bw)
    out.append(rolloff)
    out.append(zcr)
  
    for e in mfcc:
        val = np.mean(e)
        out.append(val)
        isvoiceout.append(val)

    for e in chroma_stft:
        out.append(np.mean(e))

    out = np.array(out).reshape(1,36)
    isvoiceout = np.array(isvoiceout).reshape(1,20)

    for model_name in model_names:
        if model_name == "isvoice":
            features_for_models[model_name] = isvoiceout
        else:
            features_for_models[model_name] = out
  
    return features_for_models

# audio recognition
def classifyAudio(models, scalers, features_for_models):
    result = {}
    result["status"] = "no_speech"
    if not isSoundContainsSpeech(models["isvoice"], scalers["isvoice"], features_for_models):
        return result

    result["status"] = "voice_found"
    model_names = models.keys()
    result = {}
    for model_name in model_names:
        if model_name == "isvoice":
            pass
        if model_name == "sex":
            start = time.time()
            resFromModule = getSexFromAudio(models[model_name], scalers[model_name], features_for_models)
            end = time.time()
            recognitionTime = end - start
            result[model_name] = {"recognition_result" : resFromModule, 'recognition_time' : recognitionTime }
        if model_name == "age":
            start = time.time()
            resFromModule = getAgeFromAudio(models[model_name], scalers[model_name], features_for_models)
            end = time.time()
            recognitionTime = end - start
            result[model_name] = {"recognition_result" : resFromModule, 'recognition_time' : recognitionTime }
        if model_name == "mood":
            start = time.time()
            resFromModule = getMoodFromAudio(models[model_name], scalers[model_name], features_for_models)
            end = time.time()
            recognitionTime = end - start
            result[model_name] = {"recognition_result" : resFromModule, 'recognition_time' : recognitionTime }
        if model_name == "temp":
            start = time.time()
            resFromModule = getTempFromAudio(models[model_name], scalers[model_name], features_for_models)
            end = time.time()
            recognitionTime = end - start
            result[model_name] = {"recognition_result" : resFromModule, 'recognition_time' : recognitionTime }
        if model_name == "intelligibility":
            start = time.time()
            resFromModule = getIntelligibilityFromAudio(models[model_name], scalers[model_name], features_for_models)
            end = time.time()
            recognitionTime = end - start
            result[model_name] = {"recognition_result" : resFromModule, 'recognition_time' : recognitionTime }
    return result

# audio recognition only mood
def classifyAudioOnlyMood(models, scalers, features_for_models):
    result = {}
    result["status"] = "no_speech"
    if not isSoundContainsSpeech(models["isvoice"], scalers["isvoice"], features_for_models):
        return result

    result["status"] = "voice_found"
    model_names = models.keys()

    for model_name in model_names:
        if model_name == "isvoice":
            pass
        if model_name == "sex":
            pass
        if model_name == "age":
            pass
        if model_name == "mood":
            start = time.time()
            resFromModule = getMoodFromAudio(models[model_name], scalers[model_name], features_for_models)
            end = time.time()
            recognitionTime = end - start
            result[model_name] = {"recognition_result" : resFromModule, 'recognition_time' : recognitionTime }
        if model_name == "temp":
            pass
        if model_name == "intelligibility":
            pass
    return result

def classifyAudioMoodFromPretrainedSpeechExists(models, scalers, features_for_models):
    result = {}
    result["status"] = "no_speech"

    if not isSoundContainsSpeech(models["isvoice"], scalers["isvoice"], features_for_models):
        return result

    result["status"] = "voice_found"

    return result

def get_signal_by_features(features, scaler, model):
    out = np.reshape(features,(1,-1))
    out = scaler.transform(out)
    out = np.reshape(out,(1, 64, 130))

    with tf.device('/cpu:0'):
        y = model.predict(out)
    return (np.argmax(y), str(np.max(y)))

def isSoundContainsSpeech(model, scaler, features):
    (res, confidence) = get_signal_by_features(features, scaler, model)

    answer = False
    if res == 0:
        answer = True
    return answer

def getSexFromAudio(model, scaler, features):
    (res, confidence) = get_signal_by_features(features, scaler, model)
    
    answer = 'female'
    if res == 0:
        answer = 'male'
    return { 'answer' : answer, 'confidence' : confidence }

def getAgeFromAudio(model, scaler, features):
    (res, confidence) = get_signal_by_features(features, scaler, model)

    answer = 'old'
    if res == 0:
        answer = 'adult'
    return { 'answer' : answer, 'confidence' : confidence }

def getMoodFromAudio(model, scaler, features):
    (res, confidence) = get_signal_by_features(features, scaler, model)

    answer = 'normal or happy'
    if res == 0:
        answer = 'sad or angry'
    return { 'answer' : answer, 'confidence' : confidence }

def getTempFromAudio(model, scaler, features):
    (res, confidence) = get_signal_by_features(features, scaler, model)

    answer = 'fast'
    if res == 0:
        answer = 'normal'
    return { 'answer' : answer, 'confidence' : confidence }

def getIntelligibilityFromAudio(model, scaler, features):
    (res, confidence) = get_signal_by_features(features, scaler, model)

    answer = 'normal'
    if res == 0:
        answer = 'bad'
    return { 'answer' : answer, 'confidence' : confidence }
