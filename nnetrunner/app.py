from flask import Flask
from flask import request
from flask import jsonify
from werkzeug.utils import secure_filename
import os
import time
from netmodel import *
from merthesisapp import *

app = Flask(__name__)

WIN_DEBUG = False
HOST = '0.0.0.0'
PORT = 4444

SOUND_DEFS = ["isvoice"] #["isvoice", "sex", "age", "mood", "temp", "intelligibility"]
MODELS = {}
SCALERS = {}

appeal_classification = False

# Temp folders
if WIN_DEBUG:
    tmpfilesFolder = ''
else:
    tmpfilesFolder = ''

if not os.path.exists(tmpfilesFolder):
    os.makedirs(tmpfilesFolder)

# Load models and scalers
for method in SOUND_DEFS:
    if WIN_DEBUG:
        scaler_path = '_' + method + '_recognition.bin'
        model_path = '_' + method + '_recognition.h5'
    else:
        scaler_path = './scalers/std_scaler_' + method + '_recognition.bin'
        model_path = './models/model_' + method + '_recognition.h5'

    MODELS[method] = getSoundDefinitionModel(model_path)
    SCALERS[method] = getScaller(scaler_path)

# appeals classification
if WIN_DEBUG:
    model_appeals_classification_path = ''
    map_appeals_classification_path = ''
    appeals_texts_for_tekenizer = ''
else:
    model_appeals_classification_path = './models/model_txt_appeals.h5'
    map_appeals_classification_path = './maps/appeals_classes.npy'
    appeals_texts_for_tekenizer = './data/appeals.csv'

if appeal_classification:
    model_appeal_classification = getAppealClassificationModel(model_appeals_classification_path)
    appeal_categories = getCategoriesMap(map_appeals_classification_path)
    tokenizer = getAppealsTokenizer(appeals_texts_for_tekenizer)

# yamnet pretrained model
ser_yamnet_model_ravdess = YAMNetSERModel(SER_YAMNET_MODEL_RAVDESS, YAMNET_URL, emotions_ravdess, input_length_ravdess, SAMPLING_RATE)

print('models loaded')

# api
@app.route('/classifysound', methods = ['POST'])
def classifysound():
    try:
        if request.method == 'POST':
            f = request.files['file']
            fpath = tmpfilesFolder + secure_filename(f.filename)
            f.save(fpath)

            (features_for_models, _ , preprocess_time) = get_input_for_models(fpath, False)

            start = time.time()
            ans = classifyAudio(MODELS, SCALERS, features_for_models)
            end = time.time()
            
            elapsed = end - start
            return jsonify({'classifiedanswer': ans, 'elapsed' : elapsed, 'elapsed_preprocessed' : preprocess_time})
    except Exception as exception:
        print(exception)
        response = jsonify({'error': 'problem predicting'})
        response.status_code = 400
        return response

@app.route('/classifysoundmood', methods = ['POST'])
def classifysoundmood():
    try:
        if request.method == 'POST':
            f = request.files['file']
            fpath = tmpfilesFolder + secure_filename(f.filename)
            f.save(fpath)

            (features_for_models, _ , preprocess_time) = get_input_for_models(fpath, False)

            start = time.time()

            result = classifyAudioMoodFromPretrainedSpeechExists(MODELS, SCALERS, features_for_models)
            if result["status"] == "voice_found":

                # Yamnet predictions
                (pretrainedAnswer, confidence) = ser_yamnet_model_ravdess.predict_emotion(fpath)
                
                answer = 'normal or happy'
                conf = 1.0
                if pretrainedAnswer == 'angry':
                    answer = 'sad or angry'
                    conf = confidence

                resFromModule = { 'answer' : answer, 'confidence' : str(conf) }
                result["mood"] = {"recognition_result" : resFromModule, 'recognition_time' : 0 }

            end = time.time()
            
            elapsed = end - start
            return jsonify({'classifiedanswer': result, 'elapsed' : elapsed, 'elapsed_preprocessed' : preprocess_time})
    except Exception as exception:
        print(exception)
        response = jsonify({'error': 'problem predicting'})
        response.status_code = 400
        return response

@app.route('/classifysoundmoodold', methods = ['POST']) # classify mood using own model
def classifysoundmoodold():
    try:
        if request.method == 'POST':
            f = request.files['file']
            fpath = tmpfilesFolder + secure_filename(f.filename)
            f.save(fpath)

            (features_for_models, _ , preprocess_time) = get_input_for_models(fpath, False)

            start = time.time()
            ans = classifyAudioOnlyMood(MODELS, SCALERS, features_for_models)
            end = time.time()
            
            elapsed = end - start
            return jsonify({'classifiedanswer': ans, 'elapsed' : elapsed, 'elapsed_preprocessed' : preprocess_time})
    except Exception as exception:
        print(exception)
        response = jsonify({'error': 'problem predicting'})
        response.status_code = 400
        return response

@app.route('/classifysounddisc', methods = ['POST'])
def classifysounddisc():
    try:
        if request.method == 'POST':
            f = request.files['file']
            fpath = tmpfilesFolder + secure_filename(f.filename)
            f.save(fpath)

            (features_for_models, lowDetailedFeatures, preprocess_time) = get_input_for_models(fpath, True)

            start = time.time()
            ans = classifyAudio(MODELS, SCALERS, features_for_models)
            end = time.time()
            
            elapsed = end - start
            return jsonify({'classifiedanswer': ans, 'low_detailed_features': lowDetailedFeatures, 'elapsed' : elapsed, 'elapsed_preprocessed' : preprocess_time})
    except Exception as exception:
        print(exception)
        response = jsonify({'error': 'problem predicting'})
        response.status_code = 400
        return response

@app.route('/predictsex', methods = ['POST'])
def predictsex():
    try:
        if request.method == 'POST':
            f = request.files['file']
            fpath = tmpfilesFolder + secure_filename(f.filename)
            f.save(fpath)

            (features_for_models, _ , preprocess_time) = get_input_for_models(fpath, False)

            start = time.time()
            ans = getSexFromAudio(MODELS["sex"], SCALERS["sex"], features_for_models["sex"])
            end = time.time()

            elapsed = end - start
            return jsonify({'sex_prediction': ans, 'elapsed' : elapsed, 'elapsed_preprocessed' : preprocess_time})
    except Exception as exception:
        print(exception)
        response = jsonify({'error': 'problem predicting'})
        response.status_code = 400
        return response

@app.route('/classifyappeal', methods=['GET'])
def classifyappeal():
    try:
        text = request.args.get('text')
        ans = getAppealCategoryFromText(model_appeal_classification, appeal_categories, text, tokenizer)
        return ans, 200, {'Content-Type': 'text/css; charset=utf-8'}
    except Exception as exception:
        print(exception)
        response = jsonify({'error': 'problem classify'})
        response.status_code = 400
        return response

if __name__ == "__main__":
    print('service ready to start')
    app.run(host=HOST, port=PORT)
